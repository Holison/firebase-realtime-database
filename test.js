
var config = {
  apiKey: "",
  authDomain: "soafest-b14c9.firebaseapp.com",
  databaseURL: "https://soafest-b14c9.firebaseio.com",
  projectId: "soafest-b14c9",
  storageBucket: "soafest-b14c9.appspot.com",
  messagingSenderId: "44591134925"
};
firebase.initializeApp(config);


  // show table
  var show = document.getElementById('show_list');

  //reference show database table
  var databaseRef = firebase.database().ref('shows/');

  //reference performers database table
  var artRef = firebase.database().ref('performers/');

  //reference show database table
  var stageRef = firebase.database().ref('stage/');

  var rowIndex = 1;

  // dislay show table content
  databaseRef.once('value', function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
   var childKey = childSnapshot.key;
   var childData = childSnapshot.val();
//alert(childData.type);
   var row = show.insertRow(rowIndex);
   var cellId = row.insertCell(0);
   var cellName = row.insertCell(1);
   var cellStime = row.insertCell(2);
   var cellDesc = row.insertCell(3);
   var cellType = row.insertCell(4);
   var cellDura = row.insertCell(5);
   var cellStatus = row.insertCell(6);

   cellId.appendChild(document.createTextNode(childKey));
   cellName.appendChild(document.createTextNode(childData.art_name));
   cellStime.appendChild(document.createTextNode(childData.startTime));
   cellDesc.appendChild(document.createTextNode(childData.notes));
   cellType.appendChild(document.createTextNode(childData.type));
   cellDura.appendChild(document.createTextNode(childData.duration));
   var btn = document.createElement("BUTTON");
   btn.appendChild(document.createTextNode(childData.isLive));
   cellStatus.appendChild(btn);
   //btn.onclick = (changeStatus);

   rowIndex = rowIndex + 1;
    });
  });

  function changeStatus() {
    
  }

//btn.onclick(changeStatus);

  //Performers table
  var art = document.getElementById('art_list');

  // display performers table content
  artRef.once('value', function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
   var childKey = childSnapshot.key;
   var childData = childSnapshot.val();

   var artRowIndex = 1;
   var row = art.insertRow(artRowIndex);
   var cellId = row.insertCell(0);
   var artName = row.insertCell(1);
   var artType = row.insertCell(2);
   var picCell = row.insertCell(3);
   var descCell = row.insertCell(4);

   cellId.appendChild(document.createTextNode(childKey));
   artName.appendChild(document.createTextNode(childData.art_name));
   artType.appendChild(document.createTextNode(childData.art_type));
   picCell.appendChild(document.createTextNode(childData.art_picUrl));
   descCell.appendChild(document.createTextNode(childData.art_desc));


   artRowIndex = artRowIndex + 1;
    });
  });

  // stage table
  var stage = document.getElementById('stage_list');
  // dislay stage table content
  stageRef.once('value', function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
   var childKey = childSnapshot.key;
   var childData = childSnapshot.val();

   var stageRowIndex = 1;
   var row = stage.insertRow(stageRowIndex);
   var cellId = row.insertCell(0);
   var cellName = row.insertCell(1);
   var cellPic = row.insertCell(2);
   var cellDesc = row.insertCell(3);


   cellId.appendChild(document.createTextNode(childKey));
   cellName.appendChild(document.createTextNode(childData.stage_name));
   cellPic.appendChild(document.createTextNode(childData.stage_picUrl));
   cellDesc.appendChild(document.createTextNode(childData.stage_desc));

   stageRowIndex = stageRowIndex + 1;
    });
  });


  //save show data
  function save_show(){
    var show_type = document.getElementById('show_type').value;
    var start_time = document.getElementById('start_time').value;
    var show_desc = document.getElementById('show_desc').value;
    var show_delay = document.getElementById('show_delay').value;



   var uid = firebase.database().ref().child('show').push().key;

   var data = {
     show_id: uid,
     show_type: show_type,
     start_time: start_time,
     show_desc: show_desc,
     show_delay: show_delay
   }

   var updates = {};
   updates['/show/' + uid] = data;
   firebase.database().ref().update(updates);

   alert('The show is created successfully!');
   reload_page();
  }

  // save performers data
  function save_art(){
    var art_name = document.getElementById('art_name').value;
    var art_type = document.getElementById('art_type').value;
    var art_picUrl = document.getElementById('art_picUrl').value;
    var art_desc = document.getElementById('art_desc').value;

   var uid = firebase.database().ref().child('performers').push().key;

   var data = {
     art_id: uid,
     art_name: art_name,
     art_type: art_type,
     art_picUrl: art_picUrl,
     art_desc: art_desc
   }

   var updates = {};
   updates['/performers/' + uid] = data;
   firebase.database().ref().update(updates);

   alert('The Artist is created successfully!');
   reload_page();
  }


  //save stage data
  function save_stage(){
    var stage_name = document.getElementById('stage_name').value;
    var stage_picUrl = document.getElementById('stage_picUrl').value;
    var stage_desc = document.getElementById('stage_desc').value;

   var uid = firebase.database().ref().child('stage').push().key;

   var data = {
     stage_id: uid,
     stage_name: stage_name,
     stage_picUrl: stage_picUrl,
     stage_desc: stage_desc
   }

   var updates = {};
   updates['/stage/' + uid] = data;
   firebase.database().ref().update(updates);

   alert('The stage is created successfully!');
   reload_page();
  }


  // update show list
  function update_show(){
   var show_type = document.getElementById('show_type').value;
   var start_time = document.getElementById('start_time').value;
   var show_desc = document.getElementById('show_desc').value;
   var show_delay = document.getElementById('show_delay').value;
   var show_id = document.getElementById('show_id').value;

   var data = {
    show_id: show_id,
    show_type: show_type,
    start_time: start_time,
    show_desc: show_desc,
    show_delay: show_delay
   }

   var updates = {};
   updates['/show/' + show_id] = data;
   firebase.database().ref().update(updates);

   alert('The show is updated successfully!');

   reload_page();
  }

  //update performers list
  function update_art(){
   var art_name = document.getElementById('art_name').value;
   var art_type = document.getElementById('art_type').value;
   var art_picUrl = document.getElementById('art_picUrl').value;
   var art_desc = document.getElementById('art_desc').value;
   var art_id = document.getElementById('art_id').value;

   var data = {
    art_id: art_id,
    art_type: art_type,
    art_picUrl: art_picUrl,
    art_name: art_name,
    art_desc: art_desc
   }

   var updates = {};
   updates['/performers/' + art_id] = data;
   firebase.database().ref().update(updates);

   alert('The artist is updated successfully!');

   reload_page();
  }


  // update stage list
  function update_stage(){
   var stage_name = document.getElementById('stage_name').value;
   var stage_picUrl = document.getElementById('stage_picUrl').value;
   var stage_desc = document.getElementById('stage_desc').value;
   var stage_id = document.getElementById('stage_id').value;

   var data = {
    stage_id: stage_id,
    stage_name: stage_name,
    stage_picUrl: stage_picUrl,
    stage_desc: stage_desc
   }

   var updates = {};
   updates['/stage/' + stage_id] = data;
   firebase.database().ref().update(updates);

   alert('The stage is updated successfully!');

   reload_page();
  }

  // delete show row
  function delete_show(){
   var show_id = document.getElementById('show_id').value;

   firebase.database().ref().child('/show/' + show_id).remove();
   alert('The show is deleted successfully!');
   reload_page();
  }

  // delete Performers row
  function delete_art(){
   var art_id = document.getElementById('art_id').value;

   firebase.database().ref().child('/performers/' + art_id).remove();
   alert('The artist is deleted successfully!');
   reload_page();
  }

  // delete stage row
  function delete_stage(){
   var stage_id = document.getElementById('stage_id').value;

   firebase.database().ref().child('/stage/' + stage_id).remove();
   alert('The stage is deleted successfully!');
   reload_page();
  }


  function reload_page(){
   window.location.reload();
  }
