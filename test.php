<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
 <title>SOA Festival</title>
 <script src="https://www.gstatic.com/firebasejs/4.9.0/firebase.js"></script>

 <style>
body, html {
    height: 100%;
    font-family: Arial, Helvetica, sans-serif;
}

* {
    box-sizing: border-box;
}

.dropbtn {
    background-color: #3498DB;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.show {display: block;}

.container {
  position: relative;
  text-align: left;
  color: black;
}

td,h2 {
  opacity: 1;
}
</style>

</head>
<body>



  <div class="container">
    <div class="dropdown">
      <h4> Shows Management</h4>
        <button class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">Select Stage
        <span class="caret"></span></button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
          <li role="presentation"><a role="menuitem" tabindex="-1" href="main-stage.php">Main Stage</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="chill-stage.php">Chill Stage</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="kids-stage.php">Kids Stage</a></li>
        </ul>
      </div>

  <div class="show">
  <h2> Show Data </h2>
 <table>
  <tr>
   <td>Id: </td>
   <td><input type="text" name="id" id="show_id" /></td>
  </tr>
  <tr>
   <td>Show Type: </td>
   <td><input type="text" name="show_type" id="show_type" /></td>
  </tr>
  <tr>
   <td>Start Time: </td>
   <td><input type="text" name="start_time" id="start_time" /></td>
  </tr>
  <tr>
   <td>Notes: </td>
   <td><input type="text" name="show_desc" id="show_desc" /></td>
  </tr>
  <tr>
   <td>Status: </td>
   <td><input type="text" name="show_delay" id="show_delay" /></td>
  </tr>
  <tr>
   <td colspan="2">
    <input type="button" value="Save" onclick="save_show();" />
    <input type="button" value="Update" onclick="update_show();" />
    <input type="button" value="Delete" onclick="delete_show();" />
   </td>
  </tr>
 </table>

 <h3>Show List</h3>

 <table id="show_list" border="1">
  <tr>
   <td>#ID</td>
   <td>Type</td>
   <td>Start Time</td>
   <td>Notes</td>
   <td>Status</td>
  </tr>
</table>
</div>


 <div class="art">
 <h2> Performers Data </h2>
 <table>
 <tr>
  <td>Id: </td>
  <td><input type="text" name="art_id" id="art_id" /></td>
 </tr>
 <tr>
  <td>Performer: </td>
  <td><input type="text" name="art_name" id="art_name" /></td>
 </tr>
 <tr>
  <td>Type: </td>
  <td><input type="text" name="art_type" id="art_type" /></td>
 </tr>
 <tr>
  <td>Pictute: </td>
  <td><input type="text" name="art_picUrl" id="art_picUrl" /></td>
 </tr>
 <tr>
  <td>Description: </td>
  <td><input type="text" name="art_desc" id="art_desc" /></td>
 </tr>

 <tr>
  <td colspan="2">
   <input type="button" value="Save" onclick="save_art();" />
   <input type="button" value="Update" onclick="update_art();" />
   <input type="button" value="Delete" onclick="delete_art();" />
  </td>
 </tr>
 </table>
 <h3>Artist List</h3>

 <table id="art_list" border="1">
  <tr>
   <td>#ID</td>
   <td>Performer</td>
   <td>Type</td>
   <td>picUrl</td>
   <td>Description</td>
  </tr>
 </table>
</div>

<div class="stage">
 <h2> Stage Data </h2>
 <table>
 <tr>
  <td>Id: </td>
  <td><input type="text" name="stage_id" id="stage_id" /></td>
 </tr>
 <tr>
  <td>Name: </td>
  <td><input type="text" name="stage_name" id="stage_name" /></td>
 </tr>
 <tr>
  <td>Pictute: </td>
  <td><input type="text" name="stage_picUrl" id="stage_picUrl" /></td>
 </tr>
 <tr>
  <td>Description: </td>
  <td><input type="text" name="stage_desc" id="stage_desc" /></td>
 </tr>

 <tr>
  <td colspan="2">
   <input type="button" value="Save" onclick="save_stage();" />
   <input type="button" value="Update" onclick="update_stage();" />
   <input type="button" value="Delete" onclick="delete_stage();" />
  </td>
 </tr>
 </table>
 <h3>Stage List</h3>

 <table id="stage_list" border="1">
  <tr>
   <td>#ID</td>
   <td>Name</td>
   <td>picUrl</td>
   <td>Description</td>
  </tr>
 </table>
</div>

</div>

<script src="test.js"></script>
 </body>
 </html>
