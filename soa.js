var config = {
  apiKey: "",
  authDomain: "soafest-b14c9.firebaseapp.com",
  databaseURL: "https://soafest-b14c9.firebaseio.com",
  projectId: "soafest-b14c9",
  storageBucket: "soafest-b14c9.appspot.com",
  messagingSenderId: "44591134925"
};
firebase.initializeApp(config);

// Get a reference to the database service
var database = firebase.database();

// var show = document.getElementById('show');
// var dataRef = firebase.database().ref('Activity').child("15:00");
// dataRef.on('child_added', function(snapshot){
// show.innerText = snapshot.key + ' : ' + snapshot.val();
// });

function submitClick(){

  var name = document.getElementById('name').value;
  var tel = document.getElementById('tel').value;

saveData(name,tel);
}

function saveData(name,tel){
  var firebaseRef = firebase.database().ref('update');
  //var setData = firebaseRef.push();

  firebaseRef.push().set({Name:name,Tel:tel});
  //firebaseRef.push().set({Tel:tel});
  //firebaseRef.push().set('Sam');

}

//


var rootRef = firebase.database().ref().child('Performers');
rootRef.on('child_added', snap => {
  //alert(snap.val());
  var name = snap.child('name').val();
  var desc = snap.child('description').val();
  var type = snap.child('type').val();

  document.getElementById('artist').innerText = 'Performer : ' + name;
  document.getElementById('desc').innerText = 'Description : ' + desc;

});

var actRef = firebase.database().ref().child('Activity');
actRef.on('child_added', snap => {

  var start = snap.child('startTime').val();
  var end = snap.child('endTime').val();
  var type = snap.child('type').val();

  document.getElementById('time').innerText = 'Start Time : ' + start;
  document.getElementById('show').innerText = 'Show : ' + type;

});

var actRef = firebase.database().ref().child('Stage');
actRef.on('child_added', snap => {

  var stage = snap.child('name').val();

  document.getElementById('stage').innerText = 'Stage : ' + stage;

});


$(function () {
         $(".like").click(function () {
             var input = $(this).find('.qty1');
             input.val(parseInt(input.val())+ 1);

         });
     $(".dislike").click(function () {
         var input = $(this).find('.qty2');
         input.val(input.val() - 1);
     });
  });
