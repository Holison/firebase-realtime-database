<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <title>Main Stage</title>
 <script src="https://www.gstatic.com/firebasejs/4.9.0/firebase.js"></script>
 <style>
 body, html {
    height: 100%;
    font-family: Arial, Helvetica, sans-serif;
 }

 * {
    box-sizing: border-box;
 }
 .art {display: block;}

 .container {
   position: relative;
   text-align: left;
   color: black;
 }

 h1{
   opacity: 1;
   color: green;
 }
 </style>

 </head>
 <body>
   <div class="container">
     <div class="dropdown">
       <h1> Shows Management</h1>
         <button class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">Select Stage
         <span class="caret"></span></button>
         <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
           <li role="presentation"><a role="menuitem" tabindex="-1" href="main-stage.php">Main Stage</a></li>
           <li role="presentation"><a role="menuitem" tabindex="-1" href="chill-stage.php">Chill Stage</a></li>
           <li role="presentation"><a role="menuitem" tabindex="-1" href="kids-stage.php">Kids Stage</a></li>
         </ul>
       </div>

       <div class="art">
       <h2> Chill Stage </h2>
       <table>
       <tr>
        <td>Id: </td>
        <td><input type="text" name="art_id" id="art_id" /></td>
       </tr>
       <tr>
        <td>Performer: </td>
        <td><input type="text" name="art_name" id="art_name" /></td>
       </tr>
       <tr>
        <td>Type: </td>
        <td><input type="text" name="art_type" id="art_type" /></td>
       </tr>
       <tr>
        <td>Pictute: </td>
        <td><input type="text" name="art_picUrl" id="art_picUrl" /></td>
       </tr>
       <tr>
        <td>Description: </td>
        <td><input type="text" name="art_desc" id="art_desc" /></td>
       </tr>

       <tr>
        <td colspan="2">
         <input type="button" value="Save" onclick="save_art();" />
         <input type="button" value="Update" onclick="update_art();" />
         <input type="button" value="Delete" onclick="delete_art();" />
        </td>
       </tr>
       </table>
       <h3>Performers List</h3>

       <table id="art_list" border="1">
        <tr>
         <td>#ID</td>
         <td>Performer</td>
         <td>Type</td>
         <td>picUrl</td>
         <td>Description</td>
        </tr>
       </table>
      </div>

 <script src="test.js"></script>
  </body>
  </html>
