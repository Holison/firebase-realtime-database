<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <title>Main Stage</title>
 <script src="https://www.gstatic.com/firebasejs/4.9.0/firebase.js"></script>
 <style>
 body, html {
    height: 100%;
    font-family: Arial, Helvetica, sans-serif;
 }

 * {
    box-sizing: border-box;
 }
 .show {display: block;}

 .container {
   position: relative;
   text-align: left;
   color: black;
 }

 h1{
   opacity: 1;
   color: green;
 }
 </style>

 </head>
 <body>
   <div class="container">
     <div class="dropdown">
       <h1> Shows Management</h1>
         <button class="btn btn-primary dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">Select Stage
         <span class="caret"></span></button>
         <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
           <li role="presentation"><a role="menuitem" tabindex="-1" href="main-stage.php">Main Stage</a></li>
           <li role="presentation"><a role="menuitem" tabindex="-1" href="chill-stage.php">Chill Stage</a></li>
           <li role="presentation"><a role="menuitem" tabindex="-1" href="kids-stage.php">Kids Stage</a></li>
         </ul>
       </div>

   <div class="show">
   <h2> Main Stage </h2>
  <table>
   <tr>
    <td>Name: </td>
    <td><input type="text" name="id" id="show_id" /></td>
   </tr>
   <tr>
    <td>Show Type: </td>
    <td><input type="text" name="show_type" id="show_type" /></td>
   </tr>
   <tr>
    <td>Start Time: </td>
    <td><input type="text" name="start_time" id="start_time" /></td>
   </tr>
   <tr>
    <td>Notes: </td>
    <td><input type="text" name="show_desc" id="show_desc" /></td>
   </tr>
   <tr>
    <td>Status: </td>
    <td><input type="button" name="show_delay" id="show_delay" value="pending" onclick="change()"/></td>
   </tr>
   <tr>
    <td colspan="2">
     <input type="button" value="Save" onclick="save_show();" />
     <input type="button" value="Update" onclick="update_show();" />
     <input type="button" value="Delete" onclick="delete_show();" />
    </td>
   </tr>
  </table>

  <h3>Performers List</h3>

  <table id="show_list" border="1">
   <tr>
    <td>Show No.</td>
    <td>Artist</td>
    <td>Start Time</td>
    <td>Notes</td>
    <td>Type</td>
    <td>Duration</td>
    <td>Status</td>
   </tr>
 </table>
 </div>

<script>
function change() {
  var status = document.getElementById('show_delay');
  if (status.value == "pending") {
    status.value = "live";
    // document.test.submit();
  } else {
    status.value = "pending";
  }
}
</script>
 <script src="test.js"></script>
  </body>
  </html>
