<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <title>SOAFest</title>

    <link type="text/css" rel="stylesheet" href="soa.css"/>
</head>
<body>

<h2> SOA Fest Event Schedule </h2>

  <div class="responsive">
    <div class="gallery">

        <img src="soa1.jpg" alt="#" width="600" height="400">
      <div class="desc">
        <div id="time"></div>
        </br>
        <div id="show"></div>
        </br>
        <div id="artist"></div>
        </br>
        <div id="stage"></div>
        </br>
        <div id="desc"></div>
      </div>
      <div class="container">
      <a class="like"><i class="fa fa-thumbs-o-up"></i>
          Like <input class="qty1" name="qty1" readonly="readonly" type="text" value="0" />
      </a>
      <a class="dislike"><i class="fa fa-thumbs-o-down"></i>
          Dislike <input class="qty2"  name="qty2" readonly="readonly" type="text" value="0" />
      </a>
  </div>
    </div>
  </div>


  <div class="responsive">
    <div class="gallery">

        <img src="soa2.jpg" alt="#" width="600" height="400">
      </a>
      <div class="desc">
        <div id="time">Time: </div>
        </br>
        <div id="show">Show:</div>
        </br>
        <div id="art">Performer:</div>
        <ul id="li"></ul>
        </br>
        <div id="stage">Stage</div>
        </br>
        <div id="decription">Description:</div>
      </div>
    </div>
  </div>

  <div class="responsive">
    <div class="gallery">

        <img src="soa3.jpg" alt="#" width="600" height="400">
      </a>
      <div class="desc">
        <div id="time">Time: </div>
        </br>
        <div id="show">Show:</div>
        </br>
        <div id="art">Performer:</div>
        <ul id="li"></ul>
        </br>
        <div id="stage">Stage</div>
        </br>
        <div id="decription">Description:</div>
      </div>
    </div>
  </div>

  <div class="responsive">
    <div class="gallery">

        <img src="soa4.jpg" alt="#" width="600" height="400">
      </a>
      <div class="desc">
        <div id="time">Time</div>
        </br>
        <div id="show">Show</div>
        </br>
        <div id="artist">Performer</div>
        <ul id="li"></ul>
        </br>
        <div id="stage">Stage</div>
        </br>
        <div id="decription">Description:</div>
      </div>
    </div>
  </div>

  <div class="clearfix"></div>

<br><br><br>
<form action="#" method="post" id="data-form">
<input type="text" id="name" placeholder="Name">
<input type="text" id="tel" placeholder="Tel">
<input type="submit" name="submit" value="Submit" onclick="submitClick()">
</form>

  </tbody>
</table>
<script src="https://www.gstatic.com/firebasejs/5.5.8/firebase.js"></script>
<script src="soa.js"></script>

</body>
</html>
